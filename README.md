**Description**

An API to manage movies, franchises, characters and actors in a database. Use CRUD requests for all entities, 
as well as usefull retrieval endpoints to get usefull data about each entity's relation to other entities.

**Requirements:**
- Microsoft.EntityFrameworkCore.SQLserver
- Microsoft.EntityFrameworkCore.Design
- Microsoft.EntityFrameworkCore.Tools
- Swashbuckle.AspNetCore.Swagger
- Swashbuckle.AspNetCore.SwaggerGen
- Swashbuckle.AspNetCore.SwaggerUI
- Automapper.Extensions.Microsoft.DependencyInjection

**In order to run the application:**
- Install Visual Studio 2019 community edt.: https://visualstudio.microsoft.com/downloads/
- Install requirements using nuget packet manager.
- Clone this repository to desired destination.
- Open the Visual Studio Solution file with Visual Studio.
- Set up a SQL server, and change the connectionstring in appsettings.json with your data source.
    - Could use MSSMS: https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15
- With nuget packet manager, run the command "update-database"

Run the solution with Visual Studio to view the documentation page.

With the application running, you can use postman to further test the API:

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/b275e6c9ab4d84f62d58)
