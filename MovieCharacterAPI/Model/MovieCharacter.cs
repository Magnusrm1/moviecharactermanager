﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Model
{
    public class MovieCharacter
    {
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
        public int CharacterId { get; set; }
        public Character Character { get; set; }
        public int ActorId { get; set; }
        public Actor Actor { get; set; }
        public string Picture { get; set; }
    }
}
