﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Model;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Controllers
{
    // Controller to manage the many-to-many relationship between movie and character.
    [Route("v1/api/movie-character")]
    [ApiController]
    public class MovieCharacterController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MovieCharacterController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/movie-character
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacterDto>>> GetMovieCharacters()
        {
            // Retrieve movie-characters.
            var movieCharacters = await _context.MovieCharacters.ToListAsync();

            // Map to DTOs and return result.
            return movieCharacters.Select(mc => _mapper.Map<MovieCharacterDto>(mc)).ToList();
        }

        // PUT: api/movie-character
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<IActionResult> PutMovieCharacter(MovieCharacter movieCharacter)
        {
            _context.Entry(movieCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieCharacterExists(movieCharacter.ActorId, movieCharacter.CharacterId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/movie-character
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieCharacterDto>> PostMovieCharacter(MovieCharacter movieCharacter)
        {
            // Add movie-character to context and save changes to database.
            _context.MovieCharacters.Add(movieCharacter);
            await _context.SaveChangesAsync();

            // Map the movie-character to a DTO and return it.
            var movieCharacterDto = _mapper.Map<MovieCharacterDto>(movieCharacter);

            return Ok(movieCharacterDto);
        }

        // DELETE: api/movie-character
        [HttpDelete]
        public async Task<ActionResult<MovieCharacterDto>> DeleteMovieCharacter(MovieCharacter movieCharacter)
        {
            // Find the movie-character. Return if it does not exist.
            var target = await _context.MovieCharacters.FindAsync(movieCharacter.MovieId, movieCharacter.CharacterId);
            if (target == null)
            {
                return NotFound();
            }

            // Remove the specified movie-character from context and save changes to database.
            _context.MovieCharacters.Remove(target);
            await _context.SaveChangesAsync();

            // Map to DTO and return the deleted movie-character for reference.
            var movieCharacterDto = _mapper.Map<MovieCharacterDto>(target);

            return Ok(movieCharacterDto);
        }

        private bool MovieCharacterExists(int movieId, int characterId)
        {
            return _context.MovieCharacters.Any(e => e.MovieId == movieId && e.CharacterId == characterId);
        }
    }
}
