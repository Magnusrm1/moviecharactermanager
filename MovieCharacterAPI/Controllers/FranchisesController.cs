﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Franchise;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Controllers
{
    [Route("v1/api/franchise")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/franchise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDto>>> GetFranchises()
        {
            // Retrieve Franchises.
            var franchises = await _context.Franchises.ToListAsync();

            // Map to DTOs and return result.
            return franchises.Select(f => _mapper.Map<FranchiseDto>(f)).ToList();
        }

        // GET: api/franchise/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDto>> GetFranchise(int id)
        {
            // Retrieve a specified franchise by id.
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }
            // Map to DTO and return result.
            FranchiseDto dto = _mapper.Map<FranchiseDto>(franchise);

            return Ok(dto);
        }

        

        // PUT: api/franchise/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/franchise
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FranchiseDto>> PostFranchise(Franchise franchise)
        {
            // Add franchise to context and save changes to database.
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            // Map the franchise to a DTO and return it.
            var franchiseDto = _mapper.Map<FranchiseDto>(franchise);

            return CreatedAtAction("GetFranchise", new { id = franchiseDto.Id }, franchiseDto);
        }

        // DELETE: api/franchise/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FranchiseDto>> DeleteFranchise(int id)
        {
            // Find the franchise. Return if it does not exist.
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            // Remove the specified franchise from context and save changes to database.
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            // Map to DTO and return the deleted franchise for reference.
            var franchiseDto = _mapper.Map<FranchiseDto>(franchise);

            return franchiseDto;
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
