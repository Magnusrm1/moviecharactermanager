﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Actor;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Controllers
{
    [Route("v1/api/actor")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public ActorsController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/actor
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorDto>>> GetActors()
        {
            // Retrieve actors.
            var actors = await _context.Actors.ToListAsync();

            // Map to DTOs and return result.
            return actors.Select(a => _mapper.Map<ActorDto>(a)).ToList();
        }

        // GET: api/actor/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDto>> GetActor(int id)
        {
            // Retrieve actor by id
            var actor = await _context.Actors.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }

            // Map to DTO and return result.
            ActorDto dto = _mapper.Map<ActorDto>(actor);

            return Ok(dto);
        }

        // GET: api/actor/5/characters
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterActorDto>>> GetActorCharacters(int id)
        {
            // Retrieve all MovieCharacter entries with chosen actor.
            //Include data about the relevant actor and character.
            var movieChars = await _context.MovieCharacters
                .Where(mc => mc.ActorId == id)
                .Include(mc => mc.Actor)
                .Include(mc => mc.Character)
                .ToListAsync();

            if (movieChars == null)
            {
                return NotFound();
            }

            // Map to DTO and remove duplicate data.
            IEnumerable<CharacterActorDto> characterActorDtos = movieChars.Select(
                mc => _mapper.Map<CharacterActorDto>(mc)).ToList()
                .GroupBy(cad => cad.CharacterId).Select(c => c.First());

            return Ok(characterActorDtos);
        }

        // GET: api/actor/5/movies
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieNameDto>>> GetActorMovies(int id)
        {
            // Retrieve all MovieCharacter entries for the specified actor.
            // Include data about the relevant actor and movie.
            var movieChars = await _context.MovieCharacters
                .Where(mc => mc.ActorId == id)
                .Include(mc => mc.Actor)
                .Include(mc => mc.Movie)
                .ToListAsync();

            if (movieChars == null)
            {
                return NotFound();
            }

            // Map to DTO and remove duplicate data.
            IEnumerable<MovieNameDto> movieNameDtos = movieChars.Select(
                mc => _mapper.Map<MovieNameDto>(mc)).ToList()
                .GroupBy(cad => cad.Id).Select(c => c.First());

            return Ok(movieNameDtos);
        }

        // PUT: api/actor/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/actor
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ActorDto>> PostActor(Actor actor)
        {
            // Add actor to context and save changes to database.
            _context.Actors.Add(actor);
            await _context.SaveChangesAsync();

            // Map the actor to a DTO and return it.
            var actorDto = _mapper.Map<ActorDto>(actor);

            return CreatedAtAction("GetActor", new { id = actorDto.Id }, actorDto);
        }

        // DELETE: api/actor/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ActorDto>> DeleteActor(int id)
        {
            // Find the actor. Return if it does not exist.
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            // Remove the specified actor from context and save changes to database.
            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            // Map to DTO and return the deleted actor for reference.
            var actorDto = _mapper.Map<ActorDto>(actor);

            return actorDto;
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }
    }
}
