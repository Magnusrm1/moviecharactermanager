﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Controllers
{
    [Route("v1/api/movie")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/movie
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetMovies()
        {
            // Retrieve Movies.
            var movies = await _context.Movies.ToListAsync();

            // Map to DTO and return result.
            return Ok(movies.Select(m => _mapper.Map<MovieDto>(m)).ToList());
        }

        // GET: api/movie/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetMovie(int id)
        {
            // Retrieve a specified movie by id.
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }
            // Map to DTO and return result.
            MovieDto dto = _mapper.Map<MovieDto>(movie);

            return Ok(dto);
        }

        // GET: api/movie/franchise/5
        [HttpGet("franchise/{id}")]
        public async Task<ActionResult<IEnumerable<MovieFranchiseDto>>> GetFranchiseMovies(int id)
        {
            // Retrieve all movies with the specified franchise id.
            var movies = await _context.Movies
                .Where(m => m.FranchiseId == id).ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }

            // Map to DTOs and return result.
            IEnumerable<MovieFranchiseDto> movieDtos = movies.Select(m => _mapper.Map<MovieFranchiseDto>(m));

            return Ok(movieDtos);
        }

        // GET: api/movie/5/characters-actors
        [HttpGet("{id}/characters-actors")]
        public async Task<ActionResult<IEnumerable<CharacterActorDto>>> GetMovieCharactersActors(int id)
        {
            // Retrieve all entries of moviecharacter for specified movie.
            // Include data for the relevant actors and characters. 
            var charAct = await _context.MovieCharacters
                .Include(mc => mc.Actor)
                .Include(mc => mc.Character)
                .Where(mc => mc.MovieId == id) 
                .ToListAsync();

            if (charAct == null)
            {
                return NotFound();
            }

            // Map to DTOs and return result.
            List<CharacterActorDto> characterActorDtos = charAct.Select(
                ca => _mapper.Map<CharacterActorDto>(ca)).ToList();

            /*
             * Could use this to also return which movie was requested, however this should already be known 
             * since the id needs to be inserted with the rquest.
             * In this case movieCharacterActorDto would be returned.
             
            var movie = await _context.Movies.FirstAsync(movie => movie.Id == id);
            MovieCharacterActorDto movieCharacterActorDto = new MovieCharacterActorDto()
            {
                Id = movie.Id,
                MovieTitle = movie.MovieTitle,
                CharacterActorDtos = characterActorDtos
            };
            */

            return Ok(characterActorDtos);
        }

        // PUT: api/movie/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/movie
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieDto>> PostMovie(Movie movie)
        {
            // Add movie to context and save changes to database.
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            // Map the movie to a DTO and return it.
            var movieDto = _mapper.Map<MovieDto>(movie);

            return CreatedAtAction("GetMovie", new { id = movieDto.Id }, movieDto);
        }

        // DELETE: api/movie/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieDto>> DeleteMovie(int id)
        {
            // Find the movie. Return if it does not exist.
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            // Remove the specified movie from context and save changes to database.
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            // Map to DTO and return the deleted movie for reference.
            var movieDto = _mapper.Map<MovieDto>(movie);

            return movieDto;
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
