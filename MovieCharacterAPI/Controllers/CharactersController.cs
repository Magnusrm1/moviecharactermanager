﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Controllers
{
    [Route("v1/api/character")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/character
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetCharacters()
        {
            // Retrieve Characters.
            var characters = await _context.Characters.ToListAsync();
            // Map to DTOs and return result.
            return characters.Select(c => _mapper.Map<CharacterDto>(c)).ToList();
        }

        // GET: api/character/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDto>> GetCharacter(int id)
        {
            // Return specified character by id
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            // Map to DTO and return result.
            CharacterDto dto = _mapper.Map<CharacterDto>(character);

            return Ok(dto);
        }
        
        // GET: api/character/5/actors
        [HttpGet("{id}/actors")]
        public async Task<ActionResult<IEnumerable<CharacterActorDto>>> GetCharacterActors(int id)
        {
            // Retrieve all MovieCharacter entries for a specified character.
            // Include data about the relevant actor and character.
            var movieChars = await _context.MovieCharacters
                .Where(mc => mc.CharacterId == id)
                .Include(mc => mc.Actor)
                .Include(mc => mc.Character)
                .ToListAsync();

            if (movieChars == null)
            {
                return NotFound();
            }

            // Map to DTOs and remove duplicate data.
            IEnumerable<CharacterActorDto> characterActorDtos = movieChars.Select(
                mc => _mapper.Map<CharacterActorDto>(mc)).ToList()
                .GroupBy(cad => cad.ActorId).Select(c => c.First());

            return Ok(characterActorDtos);
        }

        // GET: api/character/franchise/5
        [HttpGet("franchise/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterNameDto>>> GetFranchiseCharacters(int id)
        {
            // Retrieve all MovieCharacter entries, include data about the characters, movies and then the movies franchise. 
            // Select only the entries that contains the specified franchise.
            var movieChars = await _context.MovieCharacters
                .Include(mc => mc.Movie)
                .ThenInclude(m => m.Franchise)
                .Include(mc => mc.Character)
                .Where(mc => mc.Movie.FranchiseId == id)
                .ToListAsync();

            if (movieChars == null)
            {
                return NotFound();
            }

            // Map to DTOs and remove duplicate data
            IEnumerable<CharacterNameDto> characterNameDtos = movieChars.Select(
                mc => _mapper.Map<CharacterNameDto>(mc)).ToList()
                .GroupBy(cad => cad.Id).Select(c => c.First());

            return Ok(characterNameDtos);
        }

        // PUT: api/character/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/character
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CharacterDto>> PostCharacter(Character character)
        {
            // Add character to context and save changes to database.
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            // Map the character to a DTO and return it.
            var characterDto = _mapper.Map<CharacterDto>(character);

            return CreatedAtAction("GetCharacter", new { id = characterDto.Id }, characterDto);
        }

        // DELETE: api/character/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CharacterDto>> DeleteCharacter(int id)
        {
            // Find the character. Return if it does not exist.
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            // Remove the specified character from context and save changes to database.
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            // Map to DTO and return the deleted character for reference.
            var characterDto = _mapper.Map<CharacterDto>(character);

            return characterDto;
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
