﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class MovieCharacterProfile : Profile
    {
        public MovieCharacterProfile()
        {
            CreateMap<MovieCharacter, CharacterActorDto>()
                .ForMember(mcd => mcd.ActorName, opt => opt
                .MapFrom(mc => mc.Actor.FirstName + " " + mc.Actor.LastName))
                .ForMember(mcd => mcd.CharacterName, opt => opt
                .MapFrom(mc => mc.Character.FullName));
            CreateMap<MovieCharacter, MovieNameDto>()
                .ForMember(mnd => mnd.Id, opt => opt
                .MapFrom(mc => mc.MovieId))
                .ForMember(mnd => mnd.MovieTitle, opt => opt
                .MapFrom(mc => mc.Movie.MovieTitle));
            CreateMap<MovieCharacter, CharacterNameDto>()
                .ForMember(cnd => cnd.Id, opt => opt
                .MapFrom(mc => mc.CharacterId))
                .ForMember(cnd => cnd.FullName, opt => opt
                .MapFrom(mc => mc.Character.FullName));
            CreateMap<MovieCharacter, MovieCharacterDto>()
                .ForMember(mcd => mcd.Actor, opt => opt
                .MapFrom(mc => mc.Actor.FirstName + " " + mc.Actor.LastName))
                .ForMember(mcd => mcd.Character, opt => opt
                .MapFrom(mc => mc.Character.FullName))
                .ForMember(mcd => mcd.Movie, opt => opt
                .MapFrom(mc => mc.Movie.MovieTitle));
            
        }
    }
}
