﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Actor;
using MovieCharacterAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor, ActorDto>();
        }
    }
}
