﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Franchise;
using MovieCharacterAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDto>();
        }
    }
}
